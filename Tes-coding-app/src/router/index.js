import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AddPenjualanView from '../views/AddPenjualanView.vue'
import dashboardVuew from '../views/DashboardView.vue'
import penjualan from '../views/PenjualanView.vue'
import edit from '../views/EditPenjualanView.vue'

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: dashboardVuew
  },

  {
    path: '/penjualan',
    name: 'penjualan',
    component: penjualan
  },
  {
    path: '/AddPenjualan',
    name: 'Add',
    component: AddPenjualanView
  },
  {
    path: '/Penjualan/edit/:id?',
    name: 'EditPenjualan',
    component: edit
  },

]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
