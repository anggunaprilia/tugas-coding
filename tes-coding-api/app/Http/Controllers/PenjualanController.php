<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Penjualan;
use Illuminate\Http\Request;

class PenjualanController extends Controller
{
    public function penjualan() {
        $penjualan = Penjualan::all();
        return response()->json([
            'penjualan' => $penjualan,
            'message' => 'Penjualan',
            'code' => 200,
        ]);
    }

    public function savePenjualan(Request $request){
        $penjualan = new Penjualan();
        $penjualan->nama_barang = $request->nama_barang;
        $penjualan->stok = $request->stok;
        $penjualan->jumlah_terjual = $request->jumlah_terjual;
        $penjualan->tanggal_transaksi = $request->tanggal_transaksi;
        $penjualan->jenis_barang = $request->jenis_barang;
        $penjualan->save();
        return response()->json([
            'message' => 'data berhasil dibuat',
            'code' => 200
        ]);
    }

    public function deletePenjualan($id){
        $penjualan = Penjualan::find($id);
        if($penjualan){
            $penjualan->delete();
            return response()->json([
                'message' => 'data berhasil dihapus',
                'code' => 200
            ]);
        }else{
            return response()->json([
                'message' => "data tidak ada",
            ]);
        }
    }

    public function get_penjualan($id){
        $penjualan = Penjualan::find($id);
        return response()->json($penjualan);
    }

    public function update_penjualan($id, Request $request){
        $penjualan = Penjualan::where('id', $id)->first();
        $penjualan->nama_barang = $request->nama_barang;
        $penjualan->stok = $request->stok;
        $penjualan->jumlah_terjual = $request->jumlah_terjual;
        $penjualan->tanggal_transaksi = $request->tanggal_transaksi;
        $penjualan->jenis_barang = $request->jenis_barang;
        $penjualan->save();
        return response()->json([
            'message' => 'data berhasil diupdate',
            'code' => 200
        ]);
    }
}
