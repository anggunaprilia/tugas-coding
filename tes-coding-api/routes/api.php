<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('penjualan', [App\Http\Controllers\PenjualanController::class, 'penjualan']);
Route::post('save_penjualan', [App\Http\Controllers\PenjualanController::class, 'savePenjualan']);
Route::delete('delete_penjualan/{id}', [App\Http\Controllers\PenjualanController::class, 'deletePenjualan']);
Route::get('get_penjualan/{id}', [App\Http\Controllers\PenjualanController::class, 'get_penjualan']);
Route::post('update_penjualan/{id}', [App\Http\Controllers\PenjualanController::class, 'update_penjualan']);
